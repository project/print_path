# Print Path

Allows PDF printing of any path through Entity Print
Similar to Print Route but no knowledge of the route is needed.
Just prefix "/printpdf" to your request.

[Entity Print](https://www.drupal.org/project/entity_print)

[Print Route](https://www.drupal.org/project/print_route)

For a full description of the module, visit the
[project page](https://www.drupal.org/project/print_path).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/print_path).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

[Entity Print](https://www.drupal.org/project/entity_print)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will prevent the links from appearing. To get the links
back, disable the module and clear caches.


## Maintainers

- Devin Zuczek - [djdevin](https://www.drupal.org/u/djdevin)
