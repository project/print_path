<?php

namespace Drupal\print_path\EventSubscriber;

use Drupal;
use Drupal\Core\Render\HtmlResponse;
use Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface;
use Drupal\entity_print\Plugin\PrintEngineBase;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber for generating PDFs of any page.
 */
class PrintPdfRequestSubscriber implements EventSubscriberInterface {

  protected EntityPrintPluginManagerInterface $entityPrintPluginManager;

  public function __construct(EntityPrintPluginManagerInterface $entityPrintPluginManager) {
    $this->entityPrintPluginManager = $entityPrintPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // We need to go before any other request altering events.
      KernelEvents::REQUEST => ['onRequest', 100],
    ];
  }

  /**
   * Intercept a request, remove the "/printpdf" prefix, handle the request,
   * but then deliver it as a PDF.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   * @param $eventName
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   */
  public function onRequest(RequestEvent $event, $eventName, EventDispatcherInterface $dispatcher) {
    $uri = $event->getRequest()->server->get('REQUEST_URI');

    if (strpos($uri, '/printpdf/') === 0) {
      // Create a new request without the /printpdf prefix.
      $new_request = $event->getRequest()->duplicate();
      $newPath = str_replace('/printpdf', '', $uri);
      $new_request->server->set('REQUEST_URI', $newPath);

      // Have the kernel handle the new request.
      $response = $event->getKernel()->handle($new_request, HttpKernelInterface::SUB_REQUEST);

      if ($response) {
        // Get the HTML from the subrequest.
        $html = $response->getContent();

        // Add base HREF so relative paths work.
        $base = $event->getRequest()->getSchemeAndHttpHost();
        $html = "<base href=\"$base\">" . $html;

        // Get PDF plugin.
        $engines = Drupal::config('entity_print.settings')->get('print_engines');

        if (empty($engines['pdf_engine'])) {
          return;
        }

        /* @var $pdf_eng PrintEngineBase */
        $pdf_eng = $this->entityPrintPluginManager->createInstance($engines['pdf_engine']);

        if (empty($pdf_eng)) {
          return;
        }

        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'attachment; filename="print.pdf"');
        $pdf_eng->addPage($html);
        $response->setContent($pdf_eng->getBlob());
        $event->setResponse($response);
      }
    }
  }

}
