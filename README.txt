Allows PDF printing of any path through Entity Print

Similar to Print Route but no knowledge of the route is needed.

Just prefix "/printpdf" to your request.
